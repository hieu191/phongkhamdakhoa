/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author ACER
 */
public class UserDAO extends DBContext {


    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM [User]";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                User user = new User(rs.getInt("ID"), 
                        rs.getInt("role_id"), 
                        rs.getString("username"), 
                        rs.getString("password"), 
                        rs.getString("email"), 
                        rs.getString("phone"), 
                        rs.getString("full_name"), 
                        rs.getDate("dob"), 
                        rs.getBoolean("gender"), 
                        rs.getString("avatar"), 
                        rs.getDate("date_created"));
                list.add(user);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    public ArrayList<User> getAllUser(String username) {
        ArrayList<User> list = new ArrayList<>();
        try {
            String sql = "SELECT username, password, email, phone, full_name, dob, gender, avatar, created_date "
                    + "FROM [User]"
                    + "WHERE [username] = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User acc = new User();
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                acc.setPhone(rs.getString("phone"));
                acc.setFull_name(rs.getString("full_name"));
                acc.setDob(rs.getDate("dob"));
                acc.setGender(rs.getBoolean("gender"));
                acc.setAvatar(rs.getString("avatar"));
                acc.setCreated_date(rs.getDate("created_date"));

                list.add(acc);
            }
            return list;
        } catch (Exception e) {

        }
        return list;
    }

    public User checkLogin(String username, String password) {
        try {
            String sql = "SELECT [username], password FROM [User] WHERE [username] = ? AND password = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                User account = new User();
                account.setUsername(rs.getString("username"));
                account.setPassword(rs.getString("password"));
                return account;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //DAO for create new account
    public void insert(String account, String password) {
        String sql = "INSERT INTO [User] ([username], password) VALUES (?, ?)";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, account);
            stm.setString(2, password);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //DAO for check username

    public User checkUser(String user) {
        String sql = "SELECT [username], password FROM [User] WHERE [username] = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                User username = new User();
                username.setUsername(rs.getString("username"));
                username.setPassword(rs.getString("password"));
                return username;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void changePass(String user, String pass) {
        try {
            String sql = "UPDATE [User] SET password = ? WHERE [username] = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, pass);
            stm.setString(2, user);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {

//       new UserDAO().changePass("phuc", "21092001");
//            System.out.println(new UserDAO().getAllUser());

        System.out.println(new UserDAO().getAllUser("admin"));


    }
}
