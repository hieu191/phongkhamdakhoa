 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Option</th>
                                                <th>Age</th>
                                                <th>Gender</th>
                                                <th>Insurance ID</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Option</th>
                                                <th>Age</th>
                                                <th>Gender</th>
                                                <th>Insurance ID</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <c:forEach items="${requestScope.ls}" var="ls">
                                                <tr>
                                                    <td>${ls.name}</td>
                                                    <td>${ls.address}</td>
                                                    <td>${ls.option}</td>
                                                    <td>${ls.age}</td>
                                                    <c:if test="${ls.gender == true}">
                                                        <td>Male</td>
                                                    </c:if>
                                                    <c:if test="${ls.gender != true}">
                                                        <td>Female</td>
                                                    </c:if>
                                                    <td>${ls.code}</td>
                                                </tr>
                                            </c:forEach>

                                            <tr>
                                                <td>Garrett Winters</td>
                                                <td>Nevada,USA</td>
                                                <td>INSURANCE</td>
                                                <td>63</td>
                                                <td>Male</td>
                                                <td>TS - 34927342</td>
                                            </tr>
                                            <tr>
                                                <td>Ashton Cox</td>
                                                <td>Liverpool</td>
                                                <td>INSURANCE</td>
                                                <td>66</td>
                                                <td>Female</td>
                                                <td>IO - 234234234</td>
                                            </tr>
                                            <tr>
                                                <td>Cedric Kelly</td>
                                                <td>Netherland</td>
                                                <td>INSURANCE</td>
                                                <td>22</td>
                                                <td>Female</td>
                                                <td>KO - 234234235</td>
                                            </tr>
                                            <tr>
                                                <td>Airi Satou</td>
                                                <td>Tokyo, Japan</td>
                                                <td>NON - INSURANCE</td>
                                                <td>33</td>
                                                <td>Female</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Brielle Williamson</td>
                                                <td>London, England</td>
                                                <td>INSURANCE</td>
                                                <td>61</td>
                                                <td>Male</td>
                                                <td>UX - 2342354353</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>